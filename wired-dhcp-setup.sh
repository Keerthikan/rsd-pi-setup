#!/bin/bash
#Check for root permissions
if [[ $EUID -ne 0 ]]; then
  echo "You need to run this script with root permissions!"
  exit 1
fi


#Tie the name "eth0" to the MAC address of the Pi (Pi-dependent)
MACADDR=$(ifconfig | grep HWaddr | grep -vE 'lo|wlan0' | sed -E 's/^.*HWaddr //' | cut -d ' ' -f 1 | head -n 1)
NIC=$(ifconfig | grep $MACADDR | cut -d ' ' -f 1)

echo " --- configuring for $NIC, which has $MACADDR as physical address. Press enter --- "
read
#echo "[Match]
#MACAddress=$MACADDR
#[Link]
#Name=$NIC" > /etc/systemd/network/10-lan.link 

#Set a static IP
echo "# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

# The loopback network interface
auto lo
iface lo inet loopback

auto $NIC
iface $NIC inet static
address 192.168.3.14
netmask 255.255.0.0" > /etc/network/interfaces

echo "[main]
plugins=ifupdown,keyfile,ofono
#dns=dnsmasq

[ifupdown]
managed=true "> /etc/NetworkManager/NetworkManager.conf







### SSH Config
sed -i 's/PermitRootLogin.*$/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/X11Forwarding.*$/X11Forwarding no/g' /etc/ssh/sshd_config






# --------------------------------------------------------------------------
exit 0
# -------------------------------------------------------------------------

#Install DHCP server software
apt-get install isc-dhcp-server
#Setup the DHCP server
echo "#Mikael Westermann, 2016
#Defaults for dhcp initscript
#sourced by /etc/init.d/dhcp
#installed at /etc/default/isc-dhcp-server by the maintainer scripts
#
#This is a POSIX shell fragment
#
#On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#Separate multiple interfaces with spaces.
INTERFACES=\"$NIC\"

" > /etc/default/isc-dhcp-server

echo "#Mikael Westermann, 2016
# See http://www.ubuntugeek.com/setup-dhcp-server-on-ubuntu-16-04-xenial-xerus-server.html
#
# Sample configuration file for ISC dhcpd for Debian
#
# Attention: If /etc/ltsp/dhcpd.conf exists, that will be used as
# configuration file instead of this file.
#
#

# The ddns-updates-style parameter controls whether or not the server will
# attempt to do a DNS update when a lease is confirmed. We default to the
# behavior of the version 2 packages ('none', since DHCP v2 didn't
# have support for DDNS.)
ddns-update-style none;

# option definitions common to all supported networks...
option domain-name \"rsd1602\";
option domain-name-servers 8.8.8.8;

default-lease-time 600;
max-lease-time 7200;

subnet 192.168.3.0 netmask 255.255.255.0 {
  range 192.168.3.15 192.168.3.250;
  option subnet-mask 255.255.0.0;
  option broadcast-address 192.168.3.255;
  default-lease-time 600;
  max-lease-time 7200;
}

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
#authoritative;

# Use this to send dhcp log messages to a different log file (you also
# have to hack syslog.conf to complete the redirection).
log-facility local7;

# No service will be given on this subnet, but declaring it helps the 
# DHCP server to understand the network topology.

#subnet 10.152.187.0 netmask 255.255.255.0 {
#}

# This is a very basic subnet declaration.

#subnet 10.254.239.0 netmask 255.255.255.224 {
#  range 10.254.239.10 10.254.239.20;
#  option routers rtr-239-0-1.example.org, rtr-239-0-2.example.org;
#}

# This declaration allows BOOTP clients to get dynamic addresses,
# which we don't really recommend.

#subnet 10.254.239.32 netmask 255.255.255.224 {
#  range dynamic-bootp 10.254.239.40 10.254.239.60;
#  option broadcast-address 10.254.239.31;
#  option routers rtr-239-32-1.example.org;
#}

# A slightly different configuration for an internal subnet.
#subnet 10.5.5.0 netmask 255.255.255.224 {
#  range 10.5.5.26 10.5.5.30;
#  option domain-name-servers ns1.internal.example.org;
#  option domain-name \"internal.example.org\";
#  option subnet-mask 255.255.255.224;
#  option routers 10.5.5.1;
#  option broadcast-address 10.5.5.31;
#  default-lease-time 600;
#  max-lease-time 7200;
#}

# Hosts which require special configuration options can be listed in
# host statements.   If no address is specified, the address will be
# allocated dynamically (if possible), but the host-specific information
# will still come from the host declaration.

#host passacaglia {
#  hardware ethernet 0:0:c0:5d:bd:95;
#  filename \"vmunix.passacaglia\";
#  server-name \"toccata.fugue.com\";
#}

# Fixed IP addresses can also be specified for hosts.   These addresses
# should not also be listed as being available for dynamic assignment.
# Hosts for which fixed IP addresses have been specified can boot using
# BOOTP or DHCP.   Hosts for which no fixed address is specified can only
# be booted with DHCP, unless there is an address range on the subnet
# to which a BOOTP client is connected which has the dynamic-bootp flag
# set.

# You can declare a class of clients and then do address allocation
# based on that.   The example below shows a case where all clients
# in a certain class get addresses on the 10.17.224/24 subnet, and all
# other clients get addresses on the 10.0.29/24 subnet.

#class \"foo\" {
#  match if substring (option vendor-class-identifier, 0, 4) = \"SUNW\";
#}

#shared-network 224-29 {
#  subnet 10.17.224.0 netmask 255.255.255.0 {
#    option routers rtr-224.example.org;
#  }
#  subnet 10.0.29.0 netmask 255.255.255.0 {
#    option routers rtr-29.example.org;
#  }
#  pool {
#    allow members of \"foo\";
#    range 10.17.224.10 10.17.224.250;
#  }
#  pool {
#    deny members of \"foo\";
#    range 10.0.29.10 10.0.29.230;
#  }
#}" > /etc/dhcp/dhcpd.conf

systemctl restart isc-dhcp-server
