#!/bin/bash

### Setting up firewall

echo "# Disable all blocks" > /etc/iptables/rules.v4

netfilter-persistent reload
