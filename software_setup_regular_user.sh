#!/bin/bash
#su rsd1602
echo "Updating ROS"
rosdep init
rosdep update
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

#Installing Frobomind
echo "Installing Frobomind"
mkdir -p ~/roswork/src
cd ~/roswork/src
#git clone http://github.com/frobolab/frobomind.git
git clone http://github.com/niive12/frobomind.git
git clone http://github.com/frobolab/frobit_demo.git


cd ~/roswork/src
frobomind/frobomind_make cleanall
frobomind/frobomind_make -i /frobit_demo
frobomind/frobomind_make -i /frobit_demo
echo "source ~/roswork/devel/setup.bash" >> ~/.bashrc
echo 'PATH="$PATH:~/roswork/src"' >> ~/.bashrc
source ~/.bashrc
