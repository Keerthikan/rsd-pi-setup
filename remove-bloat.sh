#!/bin/bash
apt update

apt remove \
brasero brasero-cdrkit brasero-common \
libreoffice-avmedia-backend-gstreamer libreoffice-base-core libreoffice-calc libreoffice-common \
libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk libreoffice-impress libreoffice-ogltrans libreoffice-pdfimport \
libreoffice-style-galaxy libreoffice-style-human libreoffice-writer \
nuscratch scratch squeak-plugins-scratch squeak-vm \
minecraft-pi python-minecraftpi python-picraft python3-minecraftpi python3-picraft

apt upgrade
