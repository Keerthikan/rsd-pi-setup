#!/bin/bash

echo "# Enable DHCP and SSH on wired and block SSH on wlan0
*filter
:INPUT ACCEPT [40:16698]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [42:3312]
-A INPUT -i $NIC -p udp -m udp --sport 67:68 --dport 67:68 -j ACCEPT
-A INPUT -i $NIC -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i $NIC -j DROP
COMMIT" > /etc/iptables/rules.v4

netfilter-persistent reload
